/**
 * TODO: VCA:
 * - Implementar la opción de que el usuario me pueda pasar la lista de elementos a usar (elements), así no tendría
 * que cargarla desde dist/elements.js, crear opción en la que el usuario me pase sus elementos y estos se integren
 * con los elementos por default.
 */

angular.module("cf", [
  'formly',
  'formlyBootstrap',
  'cfDirectives',
  'dragularModule',
  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'ui.bootstrap.transition',
  'ui.bootstrap.datepicker',
  'dndLists',
  'ckeditor',
  'localytics.directives',
  'ui.mask',
  'ui.ace'
])

  //.config(['formlyConfig', 'formlyConfigProvider', 'formlyApiCheck', 'onProd', function (formlyConfig, formlyConfigProvider, formlyApiCheck, onProd) {
  //  formlyConfig.disableWarnings = onProd;
  //}])

  .run(['$rootScope', function ($rootScope) {
    // Root elements. Ex:
    $rootScope.rootElements = [];

    // Instanciate extra variable with all new elements (chosen, ckeditor, etc).
    $rootScope.extras = [];
    $rootScope.elements = [];

    // Load all extra elements.
    // TODO: VCA: Con el mecanismo nuevo quizás no haga falta poner elementos extras.
    $rootScope.$on('cf:form:extra', function (event, data) {
      $rootScope.extras.push(data);
    });

    // Load all elements to build form (inputs, selects, etc).
    $rootScope.$on('cf:form:elements', function (event, data) {
      $rootScope.elements.push(data);
    });

    // Load all root elements.
    $rootScope.$on('cf:form:root:elements', function (event, data) {
      $rootScope.rootElements = data.rootElements;
    });
  }]);