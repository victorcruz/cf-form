/**
 * Module Directives specific to app.
 */
angular.module("cfDirectives")

/**
 * Bootstrap DatePicker
 * http://angular-ui.github.io/bootstrap/
 * Example:
 *
 * <div cf-bootstrap-datepicker ng-model="dt"></div>
 *
 * The directive accepts the following attributes:
 *
 * - 'ng-model':      Scope model.
 * - 'min-date':      (Default: null) : Defines the minimum available date.
 * - 'max-date':      (Default: null) : Defines the maximum available date.
 * - 'show-weeks':    (Defaults: true) : Whether to display week numbers.
 * - 'starting-day':  (Defaults: 0) : Starting day of the week from 0-6 (0=Sunday, ..., 6=Saturday).
 * - 'init-date':     The initial date view when no model value is not specified.
 * - 'format-day':    (Default: 'dd') : Format of day in month.
 * - 'format-month':  (Default: 'MMMM') : Format of month in year.
 * - 'format-year':   (Default: 'yyyy') : Format of year in year range.
 *
 * - 'datepicker-popup-opt':    (Default: 'yyyy-MM-dd') : The format for displayed dates.
 * - 'show-button-bar':         (Default: true) : Whether to display a button bar underneath the datepicker.
 * - 'current-text':            (Default: 'Today') : The text to display for the current day button.
 * - 'clear-text':              (Default: 'Clear') : The text to display for the clear button.
 * - 'close-text':              (Default: 'Done') : The text to display for the close button.
 * - 'close-on-date-selection': (Default: true) : Whether to close calendar when a date is chosen.
 *
 * - 'required':      Is required?.
 * - 'label':         Label.
 * - 'description':   Description.
 */
  .directive('cfBootstrapDatepicker', ['$document', function ($document) {
    var strTemplate = "";
    strTemplate += "<div>";
    strTemplate += "  <label ng-if=\"label\">{{ label }}<\/label>";
    strTemplate += "";
    strTemplate += "  <p class=\"input-group\">";
    strTemplate += "    <input type=\"text\"";
    strTemplate += "           class=\"form-control\"";
    strTemplate += "           datepicker-popup";
    strTemplate += "           ng-model=\"model\"";
    strTemplate += "           min-date=\"minDate\"";
    strTemplate += "           max-date=\"maxDate\"";
    strTemplate += "           is-open=\"opened\"";
    strTemplate += "";
    strTemplate += "           show-weeks=\"{{ showWeeks }}\"";
    strTemplate += "           starting-day=\"{{ startingDay }}\"";
    strTemplate += "           min-mode=\"minMode\"";
    strTemplate += "           max-mode=\"maxMode\"";
    strTemplate += "           format-day=\"{{ formatDay }}\"";
    strTemplate += "           format-month=\"{{ formatMonth }}\"";
    strTemplate += "           format-year=\"{{ formatYear }}\"";
    strTemplate += "";
    strTemplate += "           datepicker-popup=\"{{ datepickerPopup }}\"";
    strTemplate += "           show-button-bar=\"{{ showButtonBar }}\"";
    strTemplate += "           current-text=\"{{ currentText }}\"";
    strTemplate += "           clear-text=\"{{ clearText }}\"";
    strTemplate += "           close-text=\"{{ closeText }}\"";
    strTemplate += "           close-on-date-selection=\"{{ closeOnDateSelection }}\"";
    strTemplate += "";
    strTemplate += "           date-disabled=\"disabled(date, mode)\"";
    strTemplate += "           ng-required=\"required\"\/>";
    strTemplate += "        <span class=\"input-group-btn\">";
    strTemplate += "          <button type=\"button\" class=\"btn btn-default\" ng-click=\"open($event)\"><i";
    strTemplate += "                  class=\"glyphicon glyphicon-calendar\"><\/i><\/button>";
    strTemplate += "        <\/span>";
    strTemplate += "  <\/p>";
    strTemplate += "  <p class=\"help-block\" style=\"margin-top: -7px;\" ng-if=\"description && description != 'undefined'\">{{ description }}<\/p>";
    strTemplate += "<\/div>";

    return {
      restrict: 'A',
      replace : true,

      //templateUrl: '../dist/common/directives/views/cf-bootstrap-datepicker.html',
      template: strTemplate,

      scope: {
        model      : '=?ngModel',
        minDate    : '=?minDate',
        maxDate    : '@?maxDate',
        showWeeks  : '=?showWeeks',
        startingDay: '=?startingDay',
        initDate   : '=?initDate',
        minMode    : '=?minMode',
        maxMode    : '=?maxMode',
        formatDay  : '=?formatDay',
        formatMonth: '=?formatMonth',
        formatYear : '=?formatYear',

        datepickerPopupOpt     : '=?datepickerPopup',
        showButtonBar          : '=?showButtonBar',
        currentText            : '=?currentText',
        clearText              : '=?clearText',
        closeText              : '=?closeText',
        closeOnDateSelectionOpt: '=?closeOnDateSelection',

        required   : '=?',
        label      : '@?',
        description: '@?'
      },

      controller: ['$scope', function ($scope) {
        $scope.model = angular.isDefined($scope.model) ? $scope.model : '';
        $scope.minDate = angular.isDefined($scope.minDate) ? $scope.minDate : null;
        $scope.maxDate = angular.isDefined($scope.maxDate) ? $scope.maxDate : null;
        $scope.showWeeks = angular.isDefined($scope.showWeeks) ? $scope.showWeeks : 'true';
        $scope.startingDay = angular.isDefined($scope.startingDay) ? $scope.startingDay : 0;
        $scope.initDate = angular.isDefined($scope.initDate) ? $scope.initDate : '20';
        $scope.minMode = angular.isDefined($scope.minMode) ? $scope.minMode : 'day';
        $scope.maxMode = angular.isDefined($scope.maxMode) ? $scope.maxMode : 'year';
        $scope.formatDay = angular.isDefined($scope.formatDay) ? $scope.formatDay : 'dd';
        $scope.formatMonth = angular.isDefined($scope.formatMonth) ? $scope.formatMonth : 'MMMM';
        $scope.formatYear = angular.isDefined($scope.formatYear) ? $scope.formatYear : 'yyyy';

        $scope.datepickerPopupOpt = angular.isDefined($scope.datepickerPopupOpt) ? $scope.datepickerPopupOpt : 'yyyy-MM-dd';
        $scope.showButtonBar = angular.isDefined($scope.showButtonBar) ? $scope.showButtonBar : true;
        $scope.currentText = angular.isDefined($scope.currentText) ? $scope.currentText : 'Today';
        $scope.clearText = angular.isDefined($scope.clearText) ? $scope.clearText : 'Clear';
        $scope.closeText = angular.isDefined($scope.closeText) ? $scope.closeText : 'Done';
        $scope.closeOnDateSelectionOpt = angular.isDefined($scope.closeOnDateSelectionOpt) ? $scope.closeOnDateSelectionOpt : true;

        $scope.required = angular.isDefined($scope.required) ? $scope.required : false;

        $scope.open = function ($event) {
          $event.preventDefault();
          $event.stopPropagation();

          $scope.opened = true;
        };

        //$scope.dateOptions = {
        //  formatYear : 'yy',
        //  startingDay: 1
        //};

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
          return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };
      }]
    }
  }]);