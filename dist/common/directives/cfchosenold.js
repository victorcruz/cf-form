/**
 * Module Directives specific to app.
 */
angular.module("cfDirectives")

/**
 * Directive to chosen with extra options.
 * Example:
 *
 * <div cfchosen multiple="true" class="col-xs-8 col-sm-8" ng-model="vehicle"></div>
 *
 * The directive accepts the following attributes:
 *
 * - 'ng-model':    Scope model.
 * - 'multiple':    Boolean.
 */
  .directive('cfchosenold', ['$document', function ($document) {
    var strTemplate = "";
    strTemplate += "<div class=\"cf-chosen\">";
    strTemplate += "  <div class=\"row\">";
    strTemplate += "    <div class=\"well well-sm cf-element-selected\" ng-click=\"init()\"";
    strTemplate += "         style=\"min-height : 33px; margin-bottom: 0px;background-color: #FFF;cursor: pointer;padding: 5px;border-radius: 0px;border: 1px solid #428BCA;\">";
    strTemplate += "";
    strTemplate += "      <span ng-show=\"!multiple\" class=\"cf-elements-span label label-sm label-primary\" style=\"margin-left: 2px;padding: 7px;  display: table;\">";
    strTemplate += "        {{ model[val] }}";
    strTemplate += "      <\/span>";
    strTemplate += "";
    strTemplate += "      <span ng-show=\"multiple\" class=\"cf-elements-span label label-sm label-primary\" ng-repeat=\"item in model\"";
    strTemplate += "            style=\"margin-left: 2px;padding: 7px;  display: table;\">";
    strTemplate += "        {{ item[val] }}";
    strTemplate += "        <i class=\"cf-remove-multiple-item glyphicon glyphicon-remove\" ng-click=\"removeChosenMultipleElement($index)\"";
    strTemplate += "           title=\"Remove\" style=\"margin-left: 5px;\"><\/i>";
    strTemplate += "      <\/span>";
    strTemplate += "";
    strTemplate += "      <!--<i class=\"cf-chosen-icon glyphicon glyphicon-chevron-down pull-right\" style=\"margin-top: -11px;\"><\/i>-->";
    strTemplate += "    <\/div>";
    strTemplate += "";
    strTemplate += "    <div class=\"cf-form-select\" style=\"position: absolute;top: 33px;z-index: 100;\">";
    strTemplate += "      <div class=\"input-group\">";
    strTemplate += "        <input class=\"cf-chosen-search form-control input-mask-product\" type=\"text\" ng-model=\"search\"";
    strTemplate += "               style=\"border: 1px solid #428BCA;border-radius: 0px;\">";
    strTemplate += "        <span class=\"input-group-addon\"";
    strTemplate += "              style=\"color: #fff;background-color: #428BCA;border-bottom-color: #428BCA;border: 1px solid #428BCA;border-radius: 0px;\">";
    strTemplate += "            <i class=\"glyphicon glyphicon-search\"><\/i>";
    strTemplate += "        <\/span>";
    strTemplate += "      <\/div>";
    strTemplate += "";
    strTemplate += "      <div>";
    strTemplate += "        <ul class=\"cf-menu\" role=\"menu\">";
    strTemplate += "          <li class=\"cf-element\" role=\"menuitem\" ng-repeat=\"item in items | filter:search\"";
    strTemplate += "              ng-click=\"thisSelect(item); thisSelectLink($index)\">{{ item[val] }}";
    strTemplate += "          <\/li>";
    strTemplate += "        <\/ul>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "  <\/div>";
    strTemplate += "<\/div>";

    return {
      restrict  : 'A',
      replace   : true,
      //templateUrl: function (elem, attrs) {
      //  return attrs.templateUrl || '../dist/cfchosen.html'
      //},
      template  : strTemplate,
      scope     : {
        model   : '=ngModel',
        items   : '=',
        multiple: '='
      },
      link      : function (scope, element, attrs, timeout) {
        var $elementSelected = element.find('.cf-element-selected');
        var $searchInput = element.find('.cf-chosen-search');
        var $formSelect = element.find('.cf-form-select');
        var $iconUpDown = element.find('.cf-chosen-icon');

        function cfchosenFormShow() {
          $iconUpDown.removeClass('glyphicon-chevron-down');
          $iconUpDown.addClass('glyphicon-chevron-up');
          $formSelect.show();
          $elementSelected.removeClass('closed');
          $elementSelected.addClass('opened');
          cfchosenUpdateAllDom();
        }

        function cfchosenFormHide() {
          $iconUpDown.removeClass('glyphicon-chevron-up');
          $iconUpDown.addClass('glyphicon-chevron-down');
          $formSelect.hide();
          $elementSelected.removeClass('opened');
          $elementSelected.addClass('closed');
        }

        function cfchosenUpdateAllDom() {
          element.find('.cf-element').each(function () {
            jQuery(this).removeClass('ui-state-disabled');
          })

          if (scope.multiple) {
            angular.forEach(scope.items, function (value, key) {
              angular.forEach(scope.model, function (valueModel) {
                if (_.isEqual(valueModel, value)) {
                  element.find('.cf-element').eq(key).addClass('ui-state-disabled');
                }
              })
            });
          } else {
            angular.forEach(scope.items, function (value, key) {
              if (_.isEqual(scope.model, value)) {
                element.find('.cf-element').eq(key).addClass('ui-state-disabled');
              }
            });
          }
        }

        cfchosenFormHide();

        element.bind('click', function (e) {
          element.find('.cf-remove-multiple-item').bind('click', function (et) {
            cfchosenUpdateAllDom();
            et.stopPropagation();
          });
        });

        $searchInput.bind('click', function (e) {
          e.stopPropagation();
        });

        // When click to select a element.
        $elementSelected.bind('click', function (e) {
          if ($elementSelected.hasClass('closed')) {
            cfchosenFormShow();
          } else {
            cfchosenFormHide();
          }

          e.stopPropagation();
        });

        scope.thisSelectLink = function (index) {
          cfchosenUpdateAllDom();
          cfchosenFormHide();
        }

        scope.$watch('items', function () {
          cfchosenUpdateAllDom();
        }, true);

        scope.$watch('search', function () {
          cfchosenUpdateAllDom();
        }, true);

        $document.on('click', function () {
          cfchosenFormHide();
        });

      },
      controller: ['$scope', '$parse', '$timeout', function ($scope) {
        $scope.search = '';

        $scope.val = 'title';

        if ($scope.multiple) {
          $scope.model = [];
        }

        $scope.thisSelect = function (item) {
          if ($scope.multiple) {
            if (!_.findWhere($scope.model, _.omit(item, '$$hashKey'))) {
              $scope.model.push(item);
            } else {
              console.log({type: 'warning', text: 'El valor ya fué seleccionado.'});
            }
          } else {
            $scope.model = item;
          }
        }

        $scope.removeChosenMultipleElement = function (index) {
          $scope.model.splice(index, 1);
        }

        //$scope.$watch('search', function () {
        //  console.log('execute search')
        //}, true);
      }]
    }
  }])