/**
 * Module Directives specific to app.
 */
angular.module("cfDirectives")

/**
 * Directive to chosen with extra options.
 * Example:
 *
 * <div cfselect ng-model="modelSelect"></div>
 *
 * The directive accepts the following attributes:
 *
 * - 'ng-model':    Scope model.
 * - 'multiple':    Boolean.
 */
  .directive('cfselect', ['$document', function ($document) {
    var strTemplate = "";
    strTemplate += "<div>";
    strTemplate += "  <!--<pre>{{ model | json }}<\/pre>-->";
    strTemplate += "  <div class=\"row\">";
    strTemplate += "    <div class=\"col-xs-12 col-lg-12\">";
    strTemplate += "      <label>";
    strTemplate += "        <translate>Add new Item<\/translate>";
    strTemplate += "      <\/label>";
    strTemplate += "      <div class=\"input-group\">";
    strTemplate += "      <span class=\"input-group-btn\">";
    strTemplate += "        <button class=\"btn btn-primary\" type=\"button\" ng-click=\"newItem(name)\">";
    strTemplate += "          <i class=\"glyphicon glyphicon-plus\"><\/i>";
    strTemplate += "        <\/button>";
    strTemplate += "      <\/span>";
    strTemplate += "        <input type=\"text\" class=\"form-control\" placeholder=\"Add new Item's Name\" ng-model=\"name\">";
    strTemplate += "      <\/div>";
    strTemplate += "      <!-- \/input-group -->";
    strTemplate += "    <\/div>";
    strTemplate += "  <\/div>";
    strTemplate += "";
    strTemplate += "  <div class=\"cf-drag-and-drop-lists\">";
    strTemplate += "    <ul dnd-list=\"model\">";
    strTemplate += "      <li ng-repeat=\"item in model\"";
    strTemplate += "          style=\"cursor: move;\"";
    strTemplate += "          dnd-draggable=\"item\"";
    strTemplate += "          dnd-moved=\"model.splice($index, 1)\"";
    strTemplate += "          dnd-effect-allowed=\"move\"";
    strTemplate += "          dnd-selected=\"options.selected = item.name\"";
    strTemplate += "          ng-class=\"{'selected': options.selected == item.name}\"";
    strTemplate += "              >";
    strTemplate += "        {{ item.name }}";
    strTemplate += "        <small ng-show=\"item.group\" title=\"Group: {{ item.group }}\">";
    strTemplate += "          <strong> - {{ item.group }}<\/strong>";
    strTemplate += "        <\/small>";
    strTemplate += "        <small>";
    strTemplate += "          <i class=\"glyphicon \" ng-class=\"{'glyphicon-edit': item.group, 'glyphicon-plus': !item.group}\"";
    strTemplate += "             title=\"Add or Edit Group\"";
    strTemplate += "             ng-init=\"showEditGroup = false\" style=\"cursor: pointer;\"";
    strTemplate += "             ng-click=\"showEditGroup = showEditGroup ? false : true\"><\/i>";
    strTemplate += "        <\/small>";
    strTemplate += "        <button type=\"button\" class=\"btn btn-danger pull-right\" style=\"margin-top: -7px;\" ng-click=\"remove($index)\">";
    strTemplate += "          <i class=\"glyphicon glyphicon-trash\"><\/i>";
    strTemplate += "        <\/button>";
    strTemplate += "";
    strTemplate += "        <div ng-show=\"showEditGroup\">";
    strTemplate += "          <div class=\"page-header\" style=\"margin-top: 0px;padding-bottom: 6px;\">";
    strTemplate += "            <small>";
    strTemplate += "              <translate>Select a group if you want or add new<\/translate>";
    strTemplate += "              :";
    strTemplate += "            <\/small>";
    strTemplate += "            <input type=\"text\" ng-model=\"newGroup\"\/>";
    strTemplate += "            <i class=\"glyphicon glyphicon-plus\" ng-click=\"addNewGroup(newGroup, $index);newGroup = ''\"";
    strTemplate += "               style=\"cursor: pointer;\"><\/i>";
    strTemplate += "            <i class=\"glyphicon glyphicon-minus\" title=\"Item without group\"";
    strTemplate += "               ng-click=\"withoutGroup($index);showEditGroup = false;\" style=\"cursor: pointer;color: #CD4436;\"><\/i>";
    strTemplate += "          <\/div>";
    strTemplate += "";
    strTemplate += "          <div style=\"margin-top: 13px;\">";
    strTemplate += "            <button ng-repeat=\"group in options.groups track by $index\" type=\"button\"";
    strTemplate += "                    ng-click=\"selectThisGroup(group, item);\"";
    strTemplate += "                    style=\"margin-left: 2px;\"";
    strTemplate += "                    ng-class=\"{'active': group == item.group}\"";
    strTemplate += "                    class=\"btn btn-success btn-xs\">{{ group }}";
    strTemplate += "            <\/button>";
    strTemplate += "          <\/div>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/li>";
    strTemplate += "    <\/ul>";
    strTemplate += "  <\/div>";
    strTemplate += "<\/div>";

    return {
      restrict: 'A',
      replace : true,

      //templateUrl: '../dist/common/directives/views/cfselect.html',
      template: strTemplate,

      scope: {
        model: '=?ngModel'
      },

      controller: ['$scope', function ($scope) {
        $scope.model = angular.isDefined($scope.model) ? $scope.model : [];

        $scope.options = {
          selected: '',
          groups  : []
        };

        //$scope.model = [
        //  {
        //    "name" : "Car",
        //    "group": ""
        //  },
        //  {
        //    "name" : "Helicopter",
        //    "group": ""
        //  },
        //  {
        //    "name" : "Sport Utility Vehicle",
        //    "group": ""
        //  },
        //  {
        //    "name" : "Bicycle",
        //    "group": "low emissions"
        //  },
        //  {
        //    "name" : "Skateboard",
        //    "group": "low emissions"
        //  }
        //];

        $scope.newItem = function (name) {
          if (name) {
            $scope.model.push({
              "name" : name,
              "group": ""
            });
            $scope.name = '';
          }
        };

        $scope.$watch('model', function (newValue) {
          $scope.options.groups = _.without(_.uniq(_.pluck(newValue, 'group')), "");
        }, true);

        $scope.addNewGroup = function (newGroup, index) {
          if (newGroup) {
            $scope.model[index].group = newGroup;
          }
        };

        $scope.selectThisGroup = function (thisGroup, item) {
          angular.forEach($scope.model, function (value, index) {
            if (value.name == item.name) {
              $scope.model[index].group = thisGroup;
            }
          });
        };

        $scope.withoutGroup = function (index) {
          $scope.model[index].group = '';
        };

        $scope.remove = function (index) {
          $scope.model.splice(index, 1);
        };
      }]
    }
  }]);