/**
 * Module Directives specific to app.
 */
angular.module("cfDirectives")

/**
 * Directive to use xdan date-time-picker.
 * https://github.com/xdan/datetimepicker
 * Example:
 *
 * <div xdan-datetime-picker
 *   class="col-xs-8 col-sm-8"
 *   name="date_turn"
 *   ng-model="the_report.start"
 *   date-format="dateFormat"
 *   current-date="currentDate"></div>
 *
 * The directive accepts the following attributes:
 *
 * - 'ng-model'       : Required.
 * - 'date-format'    : Default value is 'dd-mm-yyyy'. String to date format.
 * - 'current-date'   : Default value is ''. String to current date from server. If exist then the select date will be > that current-date.
 * - 'name'           : Default value is ''. String is the name of the field to validation with ng-messages.
 * - 'required'       : Default value is ''. String to set a field as required value.
 */
  .directive('xdanDatetimePicker', [function () {
    return {
      restrict: 'A',
      replace : true,
      require : 'ngModel',
      template: '<div class="input-group">' +
      '<span class="input-group-addon">' +
      '<i class="glyphicon glyphicon-calendar">' +
      '</i>' +
      '</span>' +
      '<input class="form-control date-picker" ng-model="model" type="text" name="{{ name }}" />' +
      '</div>',
      scope   : {
        model      : '=ngModel',
        dateFormat : '=dateFormat',
        currentDate: '=?currentDate',
        name       : '@name',
        required   : '@required',
        //mask       : '=?'
      },

      link: function (scope, el) {
        var currentDateValue = (!!scope.currentDate) ? scope.currentDate : '';
        //scope.mask = angular.isDefined(scope.mask) ? scope.mask : true;

        if (currentDateValue != '') { // Si existe entonces el usuario no debe seleccionar una fecha menor a la enviada por el servidor.
          scope.$watch('model', function (newValue) {
            var valueDate = new Date(newValue);

            if (valueDate < currentDateValue) {
              scope.model = '';

              alert('La fecha seleccionada debe ser mayor a la fecha actual.');
            }
          }, true)
        }

        el.find('.date-picker').datetimepicker({
          lang  : 'es',
          inline: false,
          //mask  : true,
          format: scope.dateFormat // "Y-m-d H:i:s"
        });
      }
    }
  }]);