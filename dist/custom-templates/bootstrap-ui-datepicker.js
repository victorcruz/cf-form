/**
 * Element: AngularJS Bootstrap UI Datepicker.
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {
    var strTemplate = "";
    strTemplate += "<div>";
    strTemplate += "  <p class=\"input-group\">";
    strTemplate += "    <input type=\"text\"";
    strTemplate += "           class=\"form-control\"";
    strTemplate += "           datepicker-popup";
    strTemplate += "           ng-model=\"model[options.key]\"";
    strTemplate += "           min-date=\"to.cfoptions.minDate\"";
    strTemplate += "           max-date=\"to.cfoptions.maxDate\"";
    strTemplate += "           is-open=\"opened\"";
    strTemplate += "";
    strTemplate += "           show-weeks=\"{{ to.cfoptions.showWeeks }}\"";
    strTemplate += "           starting-day=\"{{ to.cfoptions.startingDay }}\"";
    strTemplate += "           min-mode=\"to.cfoptions.minMode\"";
    strTemplate += "           max-mode=\"to.cfoptions.maxMode\"";
    strTemplate += "           format-day=\"{{ to.cfoptions.formatDay }}\"";
    strTemplate += "           format-month=\"{{ to.cfoptions.formatMonth }}\"";
    strTemplate += "           format-year=\"{{ to.cfoptions.formatYear }}\"";
    strTemplate += "";
    strTemplate += "           datepicker-popup=\"{{ to.cfoptions.datepickerPopupOpt }}\"";
    strTemplate += "           show-button-bar=\"{{ to.cfoptions.showButtonBar }}\"";
    strTemplate += "           current-text=\"{{ to.cfoptions.currentText }}\"";
    strTemplate += "           clear-text=\"{{ to.cfoptions.clearText }}\"";
    strTemplate += "           close-text=\"{{ to.cfoptions.closeText }}\"";
    strTemplate += "           close-on-date-selection=\"{{ to.cfoptions.closeOnDateSelectionOpt }}\"";
    strTemplate += "";
    strTemplate += "           date-disabled=\"disabled(date, mode)\"";
    strTemplate += "           ng-required=\"to.cfoptions.required\"\/>";
    strTemplate += "        <span class=\"input-group-btn\">";
    strTemplate += "          <button type=\"button\" class=\"btn btn-default\" ng-click=\"open($event)\"><i";
    strTemplate += "                  class=\"glyphicon glyphicon-calendar\"><\/i><\/button>";
    strTemplate += "        <\/span>";
    strTemplate += "  <\/p>";
    strTemplate += "<\/div>";

    var element = {
      name          : 'bootstrap-ui-datepicker',
      //extends       : 'input',
      wrapper       : ['bootstrapLabel', 'bootstrapHasError'],
      //templateUrl   : '../dist/custom-templates/views/bootstrap-ui-datepicker.html',
      template      : strTemplate,
      defaultOptions: {
        templateOptions: {
          label    : 'Set the name of the field label',
          cfoptions: {
            minDate    : null,
            maxDate    : null,
            showWeeks  : true,
            startingDay: 0,
            initDate   : 20,
            minMode    : 'day',
            maxMode    : 'year',
            formatDay  : 'dd',
            formatMonth: 'MMMM',
            formatYear : 'yyyy',

            datepickerPopupOpt     : 'yyyy-MM-dd',
            showButtonBar          : true,
            currentText            : 'Today',
            clearText              : 'Clear',
            closeText              : 'Done',
            closeOnDateSelectionOpt: true,

            required: false
          },
          list     : []
        }
      },
      link          : function (scope, el, attrs) {
      },
      controller    : ['$scope', function ($scope) {

        $scope.open = function ($event) {
          $event.preventDefault();
          $event.stopPropagation();

          $scope.opened = true;
        };

        //$scope.dateOptions = {
        //  formatYear : 'yy',
        //  startingDay: 1
        //};

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
          return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };
      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }]);