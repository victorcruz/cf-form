/**
 * Element: Example.
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {
    var element = {
      name          : 'ipAddress',
      extends       : 'input',
      //wrapper       : 'someWrapper',
      defaultOptions: {},
      link          : function (scope, el, attrs) {
        console.log('ok set type ok')
      },
      controller    : ['$scope', function ($scope) {
      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }])