/**
 * Element: UI Mask.
 * https://github.com/angular-ui/ui-mask
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {
    var element = {
      name          : 'ui-mask',
      //extends       : 'input',
      wrapper       : ['bootstrapLabel', 'bootstrapHasError'],
      template      : '<input class="form-control" type="text" ng-model="model[options.key]" ui-mask="{{ to.cfoptions.uiMask }}"/>',
      defaultOptions: {
        templateOptions: {
          label    : '',
          cfoptions: {
            uiMask: ''
          }
        }
      },
      link          : function (scope, el, attrs) {
      },
      controller    : ['$scope', function ($scope) {

      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }])

/**
 * Doc.
 */

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {

    var strTemplate = "";
    strTemplate += "<div>";
    strTemplate += "  <div class=\"panel panel-default\">";
    strTemplate += "    <div class=\"panel-heading\">";
    strTemplate += "      <h5 class=\"panel-title\" style=\"font-size: 14px;\"><strong>{{ to.label }}<\/strong><\/h5>";
    strTemplate += "    <\/div>";
    strTemplate += "    <div class=\"panel-body\">";
    strTemplate += "";
    strTemplate += "      <p>";
    strTemplate += "        Apply a mask on an input field so the user can only type pre-determined pattern. Thanks: <a";
    strTemplate += "              href=\"https:\/\/github.com\/angular-ui\/ui-mask\">Angular UI Mask<\/a>";
    strTemplate += "      <\/p>";
    strTemplate += "";
    strTemplate += "      <div class=\"row\">";
    strTemplate += "        <div class=\"col-md-6\">";
    strTemplate += "          <div class=\"form-group\">";
    strTemplate += "            <label class=\"control-label\">Mask Definition<\/label>";
    strTemplate += "            <input type=\"text\" class=\"form-control\" ng-model=\"model[options.key]\" placeholder=\"(999) 999-9999\">";
    strTemplate += "          <span class=\"help-inline\">";
    strTemplate += "              <code>A<\/code> Any letter.<br>";
    strTemplate += "              <code>9<\/code> Any number.<br>";
    strTemplate += "              <code>*<\/code> Any letter or number.<br>";
    strTemplate += "              <code>?<\/code> Make any part of the mask optional.";
    strTemplate += "          <\/span>";
    strTemplate += "          <\/div>";
    strTemplate += "        <\/div>";
    strTemplate += "";
    strTemplate += "        <div class=\"col-md-6\">";
    strTemplate += "          <div class=\"form-group\">";
    strTemplate += "            <label class=\"control-label\">Predefined masks<\/label>";
    strTemplate += "";
    strTemplate += "            <p>";
    strTemplate += "              <a href=\"\" ng-click=\"model[options.key] = undefined\">Set to undefined (uninitialize)<\/a>";
    strTemplate += "            <\/p>";
    strTemplate += "";
    strTemplate += "            <p>";
    strTemplate += "              <a href=\"\" ng-click=\"model[options.key] = '(999) 999-9999'\">Set to (999) 999-9999<\/a>";
    strTemplate += "            <\/p>";
    strTemplate += "";
    strTemplate += "            <p>";
    strTemplate += "              <a href=\"\" ng-click=\"model[options.key] = '(999) 999-9999 ext. 999'\">Set to (999) 999-9999 ext. 999<\/a>";
    strTemplate += "            <\/p>";
    strTemplate += "";
    strTemplate += "            <p>";
    strTemplate += "              <a href=\"\"";
    strTemplate += "                 ng-click=\"model[options.key] = '(999) 999-9999 ext. ?9?9?9'\">Set to (999) 999-9999 ext. ?9?9?9<\/a>";
    strTemplate += "            <\/p>";
    strTemplate += "";
    strTemplate += "            <p>";
    strTemplate += "              <a href=\"\" ng-click=\"model[options.key] = '(**: AAA-999)'\">Set to (**: AAA-999)<\/a>";
    strTemplate += "            <\/p>";
    strTemplate += "          <\/div>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "  <\/div>";
    strTemplate += "<\/div>";

    var element = {
      name          : 'doc-ui-mask',
      //extends       : 'input',
      //wrapper       : ['bootstrapLabel', 'bootstrapHasError'],
      //templateUrl   : '../dist/custom-templates/views/ui-mask-doc.html',
      template      : strTemplate,
      defaultOptions: {
        templateOptions: {
          label: '',
        }
      },
      link          : function (scope, el, attrs) {
      },
      controller    : ['$scope', function ($scope) {

      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }]);