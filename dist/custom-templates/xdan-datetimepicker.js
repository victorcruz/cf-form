/**
 * Element: Xdan DateTimePicker.
 * https://github.com/xdan/datetimepicker
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {

    var element = {
      name          : 'xdan-datetimepicker',
      //extends       : 'input',
      wrapper       : ['bootstrapLabel', 'bootstrapHasError'],
      template      : '<div xdan-datetime-picker ' +
      'ng-model="model[options.key]" ' +
      'date-format="to.cfoptions.dateFormat" ' +
      'current-date="to.cfoptions.currentDate">' +
      '</div>',
      defaultOptions: {
        templateOptions: {
          label    : '',
          cfoptions: {
            dateFormat : 'Y-m-d H:i:s',
            currentDate: ''
          }
        }
      },
      link          : function (scope, el, attrs) {
      },
      controller    : ['$scope', function ($scope) {
      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }]);