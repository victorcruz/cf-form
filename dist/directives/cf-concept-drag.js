/**
 * Module Directives specific to app.
 */
angular.module("cf")

/**
 * Directive to dragdrop.
 * Example:
 *
 * <div cfconceptdrag ng-model="concept"></div>
 *
 * Example of one concept:
 *
 * {
        "data_concept": {                   // Json. Data from cfconceptdrag directive for concept build.
          "options": {
            "change": 1444673517354.3542
          },
          "rootList": [],
          "model": []
        },
        "name": "First Concept",            // Name of concept.
        "children": [],                     // Children. This array is always empty and only instance in the tree of the version control.
        "id": 1444672909934,                // ID of concept.
        "parent": 1444672863831,            // ID from parent of the concept. When 0 it means that the concept doesn't have parent.
        "is_new_version": true              // Is this a new version of the concept?. When false it means that the concept be not shown
                                               in the list of versions because the content inherited of the father was not modified,
                                               it therefore is not a new version.
   }
 *
 * The directive accepts the following attributes:
 *
 * - 'ng-model':    Scope model.
 */
  .directive('cfconceptdrag', function () {
    var strTemplate = "";
    strTemplate += "<div class=\"row\">";
    strTemplate += "  <div>";
    strTemplate += "    <div ng-if=\"!elements || elements.length == 0\" class=\"alert alert-warning\">";
    strTemplate += "      <translate>Not exist elements to build the concept.<\/translate>";
    strTemplate += "    <\/div>";
    strTemplate += "";
    strTemplate += "    <div class=\"cf-drag-tab\">";
    strTemplate += "      <!-- Nav tabs -->";
    strTemplate += "      <ul class=\"nav nav-tabs\">";
    strTemplate += "        <li ng-class=\"{'active': $index == 0}\"";
    strTemplate += "            ng-repeat=\"element in elements track by $index\"";
    strTemplate += "            ng-click=\"selectTabElement(element)\">";
    strTemplate += "          <a href=\".tab-elements-show\" data-toggle=\"tab\" style=\"font-size: 12px;\">{{element.title}}<\/a>";
    strTemplate += "        <\/li>";
    strTemplate += "      <\/ul>";
    strTemplate += "";
    strTemplate += "      <!-- Tab panes -->";
    strTemplate += "      <div class=\"tab-content\">";
    strTemplate += "        <div class=\"tab-elements-show tab-pane fade in active\">";
    strTemplate += "          <div ng-if=\"!childrens || childrens.length == 0\" class=\"alert\" style=\"margin-bottom: 0px;\">";
    strTemplate += "            <translate>The element doesn't have components.<\/translate>";
    strTemplate += "          <\/div>";
    strTemplate += "";
    strTemplate += "          <div id=\"containerChildrens\">";
    strTemplate += "            <li type=\"button\"";
    strTemplate += "                data-parent=\"{{child.parent}}\"";
    strTemplate += "                title=\"{{ child.description }}\"";
    strTemplate += "                class=\"cf-children btn btn-default btn-sm\"";
    strTemplate += "                ng-repeat=\"child in childrens track by $index\"";
    strTemplate += "                ng-show=\"child.title\">";
    strTemplate += "              {{child.title}}";
    strTemplate += "            <\/li>";
    strTemplate += "          <\/div>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "  <\/div>";
    strTemplate += "";
    strTemplate += "  <div class=\"cf-drag-preview\"";
    strTemplate += "       ng-class=\"{'active': newChange, 'preview': preview}\"";
    strTemplate += "       ng-init=\"preview = false\"";
    strTemplate += "       ng-click=\"preview = preview ? false : true\"";
    strTemplate += "       title=\"Preview\"><\/div>";
    strTemplate += "";
    strTemplate += "  <div class=\"col-md-12 cf-dropzone\">";
    strTemplate += "";
    strTemplate += "    <div ng-show=\"preview\" class=\"row cf-drag-preview-show\">";
    strTemplate += "      <div class=\"column-preview col-xs-12 col-sm-9 col-md-9 col-lg-9\">";
    strTemplate += "        <div class=\"page-header\" style=\"margin: 12px 0 19px;padding-bottom: 6px;\">";
    strTemplate += "          <h4>";
    strTemplate += "            <translate>Preview<\/translate>";
    strTemplate += "            <small>";
    strTemplate += "              <translate>Show concept generated.<\/translate>";
    strTemplate += "            <\/small>";
    strTemplate += "          <\/h4>";
    strTemplate += "        <\/div>";
    strTemplate += "        <div class=\"row\">";
    strTemplate += "          <cfform-show ng-model=\"modelShow\" fields=\"model.model\"><\/cfform-show>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "      <div class=\"column-preview col-xs-12 col-sm-3 col-md-3 col-lg-3\">";
    strTemplate += "        <pre>{{ modelShow | json }}<\/pre>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "";
    strTemplate += "    <div ng-show=\"!preview\" id=\"containerRootList\" class=\"row\">";
    strTemplate += "      <div ng-repeat=\"item in model.rootList track by $index\"";
    strTemplate += "           class=\"cf-drag-columns {{ item.model.data.classes.text }}\"";
    strTemplate += "           ng-class=\"{'col-md-12': !item.model.data.classes.text}\">";
    strTemplate += "";
    strTemplate += "        <div class=\"cf-drag-actions-visible\">";
    strTemplate += "          <div ng-if=\"item.model.data.classes.text\" class=\"left-column\">";
    strTemplate += "            <i class=\"increasing\" ng-click=\"increasing(item, $index)\"><\/i>";
    strTemplate += "            <i class=\"decreasing\" ng-click=\"decreasing(item, $index)\"><\/i>";
    strTemplate += "          <\/div>";
    strTemplate += "";
    strTemplate += "          <div class=\"panel panel-default cf-drag-panel\">";
    strTemplate += "            <div class=\"panel-heading\">";
    strTemplate += "              <i class=\"glyphicon glyphicon-move\"><\/i> {{ item.title }}";
    strTemplate += "";
    strTemplate += "              <div class=\"pull-right\">";
    strTemplate += "                <div class=\"cf-widget-toolbar\">";
    strTemplate += "                  <div class=\"cf-drag-actions\" style=\"visibility: hidden;\">";
    strTemplate += "                    <a title=\"Minimize\/Maximize\" data-toggle=\"collapse\"";
    strTemplate += "                       ng-click=\"collapse(item.id)\"";
    strTemplate += "                       href=\".{{ item.id }}\" aria-expanded=\"false\"";
    strTemplate += "                       aria-controls=\"{{ item.id }}\">";
    strTemplate += "                      <i class=\"{{ item.id }}-toggle icon-action glyphicon glyphicon-chevron-up\"><\/i>";
    strTemplate += "                    <\/a>";
    strTemplate += "";
    strTemplate += "                    <a title=\"Edit Options\" href=\"\" ng-click=\"showEditModal(item, index)\">";
    strTemplate += "                      <i class=\"icon-action green glyphicon glyphicon-pencil\"><\/i>";
    strTemplate += "                    <\/a>";
    strTemplate += "";
    strTemplate += "                    <a title=\"Remove\" href=\"\" ng-init=\"showRemove = false\" ng-click=\"showRemove = true\">";
    strTemplate += "                      <i class=\"icon-action red glyphicon glyphicon-remove\"><\/i>";
    strTemplate += "                    <\/a>";
    strTemplate += "";
    strTemplate += "                    <div ng-show=\"showRemove\" class=\"cf-sure-remove-box\">";
    strTemplate += "                      <translate>You sure remove this?<\/translate>";
    strTemplate += "                      <button type=\"submit\" class=\"btn btn-danger btn-xs\" ng-click=\"remove($index); showRemove = false\">";
    strTemplate += "                        <translate>Yes<\/translate>";
    strTemplate += "                      <\/button>";
    strTemplate += "                      <button type=\"submit\" class=\"btn btn-default btn-xs\" ng-click=\"showRemove = false\">";
    strTemplate += "                        <translate>No<\/translate>";
    strTemplate += "                      <\/button>";
    strTemplate += "                    <\/div>";
    strTemplate += "                  <\/div>";
    strTemplate += "                <\/div>";
    strTemplate += "              <\/div>";
    strTemplate += "            <\/div>";
    strTemplate += "          <\/div>";
    strTemplate += "";
    strTemplate += "          <div class=\"{{ item.id }} collapse in\" style=\"margin-top: -2px;\">";
    strTemplate += "            <div class=\"well cf-drag-well cf-concept-mini-preview only-read\" ng-if=\"item.model.data\">";
    strTemplate += "              <div class=\"row\">";
    strTemplate += "                <formly-form fields=\"[ item.model ]\"><\/formly-form>";
    strTemplate += "              <\/div>";
    strTemplate += "            <\/div>";
    strTemplate += "";
    strTemplate += "            <div class=\"well cf-drag-well\" ng-if=\"!item.model.data\">";
    strTemplate += "              <div class=\"alert alert-without cf-drag-alert\">";
    strTemplate += "                <i class=\"glyphicon glyphicon-exclamation-sign\"><\/i>";
    strTemplate += "                <translate>Element without model, please edit this and insert data.<\/translate>";
    strTemplate += "              <\/div>";
    strTemplate += "            <\/div>";
    strTemplate += "          <\/div>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "";
    strTemplate += "      <div class=\"col-md-12\" style=\"padding: 6px;\"><\/div>";
    strTemplate += "";
    strTemplate += "      <div class=\"col-md-12 cf-dropzone-clear\" ng-hide=\"hide()\" style=\"font-size: 12px;\">";
    strTemplate += "        <div><\/div>";
    strTemplate += "        <i class=\"glyphicon glyphicon-plus\"><\/i>";
    strTemplate += "        <br>";
    strTemplate += "        <translate>Add your items here<\/translate>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "";
    strTemplate += "  <\/div>";
    strTemplate += "";
    strTemplate += "  <!-- Modal -->";
    strTemplate += "  <div class=\"cfdrag-modal modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\">";
    strTemplate += "    <div class=\"modal-dialog\" style=\"width : 61%;\">";
    strTemplate += "      <div class=\"modal-content\">";
    strTemplate += "        <div class=\"modal-header\">";
    strTemplate += "          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;<\/button>";
    strTemplate += "          <h4 class=\"modal-title\" id=\"modalLabel\">{{ headTitle }}<\/h4>";
    strTemplate += "        <\/div>";
    strTemplate += "        <div class=\"modal-body\">";
    strTemplate += "          <div class=\"row\">";
    strTemplate += "            <div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\">";
    strTemplate += "              <button type=\"button\" class=\"btn btn-default pull-right btn-small cf-option-preview\"";
    strTemplate += "                      ng-class=\"{'active': previewShow}\"";
    strTemplate += "                      ng-click=\"previewShow = previewShow ? false : true\">";
    strTemplate += "                <translate>Show Preview<\/translate>";
    strTemplate += "              <\/button>";
    strTemplate += "";
    strTemplate += "              <div ng-if=\"previewShow\" class=\"cf-modal-preview-field\">";
    strTemplate += "                <div class=\"page-header\" style=\"margin: 10px 0 11px;padding-bottom: 1px;\">";
    strTemplate += "                  <h5 style=\"margin-left: 15px;\">";
    strTemplate += "                    <translate>Preview<\/translate>";
    strTemplate += "                    <small>";
    strTemplate += "                      <translate>the field.<\/translate>";
    strTemplate += "                    <\/small>";
    strTemplate += "                  <\/h5>";
    strTemplate += "                <\/div>";
    strTemplate += "";
    strTemplate += "                <div ng-if=\"showAddPreview && action == 'add'\">";
    strTemplate += "                  <cfform-show ng-model=\"previewModelShow\" fields=\"[ modelPreview ]\"><\/cfform-show>";
    strTemplate += "                <\/div>";
    strTemplate += "";
    strTemplate += "                <div ng-if=\"showEditPreview && action == 'edit'\">";
    strTemplate += "                  <cfform-show fields=\"[ modelPreview ]\"><\/cfform-show>";
    strTemplate += "                <\/div>";
    strTemplate += "";
    strTemplate += "                <div class=\"divider cf-preview-divider\"><\/div>";
    strTemplate += "              <\/div>";
    strTemplate += "";
    strTemplate += "              <div ng-if=\"action == 'add'\">";
    strTemplate += "                <div class=\"page-header\" ng-class=\"{'margin': previewShow}\">";
    strTemplate += "                  <h5 style=\"margin-left: 15px;\">";
    strTemplate += "                    <translate>Options<\/translate>";
    strTemplate += "                    <small>";
    strTemplate += "                      <translate>of element.<\/translate>";
    strTemplate += "                    <\/small>";
    strTemplate += "                  <\/h5>";
    strTemplate += "                <\/div>";
    strTemplate += "";
    strTemplate += "                <form ng-submit=\"saveField()\">";
    strTemplate += "                  <formly-form model=\"vm.model\" fields=\"vm.modelFields\">";
    strTemplate += "                    <button class=\"btn btn-primary col-lg-12\" ng-if=\"vm.modelFields.length > 0\" type=\"submit\">";
    strTemplate += "                      <translate>Save Field<\/translate>";
    strTemplate += "                    <\/button>";
    strTemplate += "                  <\/formly-form>";
    strTemplate += "                <\/form>";
    strTemplate += "              <\/div>";
    strTemplate += "";
    strTemplate += "              <div ng-if=\"action == 'edit'\">";
    strTemplate += "                <div class=\"page-header\" ng-class=\"{'margin': previewShow}\">";
    strTemplate += "                  <h5 style=\"margin-left: 15px;\">";
    strTemplate += "                    <translate>Options<\/translate>";
    strTemplate += "                    <small>";
    strTemplate += "                      <translate>of element.<\/translate>";
    strTemplate += "                    <\/small>";
    strTemplate += "                  <\/h5>";
    strTemplate += "                <\/div>";
    strTemplate += "";
    strTemplate += "                <form ng-submit=\"editField(element.model, element.model.data.dataScheme)\">";
    strTemplate += "                  <formly-form model=\"element.model.data.dataScheme\" fields=\"element.model.data.scheme.modelFields\">";
    strTemplate += "                    <button class=\"btn btn-primary col-lg-12\"";
    strTemplate += "                            ng-if=\"element.model.data.scheme.modelFields.length > 0\" type=\"submit\">";
    strTemplate += "                      <translate>Edit Field<\/translate>";
    strTemplate += "                    <\/button>";
    strTemplate += "                  <\/formly-form>";
    strTemplate += "                <\/form>";
    strTemplate += "              <\/div>";
    strTemplate += "";
    strTemplate += "            <\/div>";
    strTemplate += "          <\/div>";
    strTemplate += "        <\/div>";
    strTemplate += "        <div class=\"modal-footer\">";
    strTemplate += "          <button type=\"button\" class=\"btn btn-default no-border\" data-dismiss=\"modal\">";
    strTemplate += "            <translate>Close<\/translate>";
    strTemplate += "          <\/button>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "  <\/div>";
    strTemplate += "";
    strTemplate += "  <p class=\"cf-drag-bottom-data\">";
    strTemplate += "    {{ model.rootList.length }}";
    strTemplate += "    <translate>fields<\/translate>";
    strTemplate += "";
    strTemplate += "    <span class=\"pull-right\"><translate>Concept Dropzone<\/translate><\/span>";
    strTemplate += "  <\/p>";
    strTemplate += "";
    strTemplate += "  <!--<div class=\"col-md-12\">-->";
    strTemplate += "  <!--<pre>{{ model.rootList | json }}<\/pre>-->";
    strTemplate += "  <!--<\/div>-->";
    strTemplate += "<\/div>";

    return {
      restrict: 'A',
      replace : true,
      require : '?ngModel',
      scope   : {
        model   : '=ngModel',
        elements: '=?'
      },

      //templateUrl: '../dist/cf-concept-drag.html',
      template: strTemplate,

      link: function (scope, element) {

        scope.collapse = function (id) {
          var $elCollapse = element.find('.' + id);
          setTimeout(function () {
            var $elToggle = element.find('.' + id + '-toggle');
            if ($elCollapse.hasClass('in')) {
              $elToggle.removeClass('glyphicon-chevron-down');
              $elToggle.addClass('glyphicon-chevron-up');
            } else {
              $elToggle.removeClass('glyphicon-chevron-up');
              $elToggle.addClass('glyphicon-chevron-down');
            }
          }, 500);
        };

        scope.selectTabElement = function (itemElement) {
          jQuery('.cf-children').each(function () {
            var $this = jQuery(this);
            var parent = JSON.parse($this.attr('data-parent'));

            if (_.contains(parent, itemElement.element)) {
              $this.show();
            } else {
              $this.hide();
            }
          });
        };

      },

      controller: ['$scope', '$rootScope', '$element', '$timeout', 'CfBuilder', 'dragularService', function ($scope, $rootScope, $element, $timeout, CfBuilder, dragularService) {
        $scope.rootElements = $rootScope.rootElements;
        $scope.extras = $rootScope.extras;

        $scope.elements = [];
        CfBuilder.getElementsChildrens($scope.rootElements, $rootScope.elements, function (result) {
          $scope.elements = result;
        });

        //$scope.model = {
        //  options : {},
        //  rootList: [],
        //  model   : []
        //};

        //----

        if (!is.empty($scope.elements)) {
          $scope.childrens = $scope.elements[0].childrens;
        }

        if (angular.isDefined($scope.model)) {
          if (!$scope.model.options || !$scope.model.rootList) {
            $scope.model = {
              options : {},
              rootList: [],
              model   : []
            };
          }
        } else {
          $scope.model = {
            options : {},
            rootList: [],
            model   : []
          };
        }

        $scope.vm = {};

        $scope.hide = function () {
          return $scope.model.rootList.length > 0;
        };

        // Block Start: Dragular drag&drop.
        var containerChildrens = document.querySelector('#containerChildrens');
        var containerRootList = document.querySelector('#containerRootList');

        self.accepts = function (el, target, source) {
          if (source === containerChildrens || source === target) {
            return true;
          }
        };

        dragularService([containerChildrens], {
          containersModel: [$scope.childrens],
          copy           : true,
          //move only from left to right
          accepts        : self.accepts
        });

        dragularService([containerRootList], {
          containersModel: [$scope.model.rootList],
          //removeOnSpill  : false,
          //move only from left to right
          accepts        : self.accepts,
          scope          : $scope,
          classes        : {
            mirror      : 'gu-mirror',
            hide        : 'gu-hide',
            unselectable: 'gu-unselectable',
            transit     : 'gu-transit',
            overActive  : 'gu-over-active',
            overAccepts : 'gu-over-accept',
            overDeclines: 'gu-over-decline'
          }
        });
        // Block End: Dragular drag&drop.

        //$scope.selectTabElement = function (element) {
        //  $scope.childrens = angular.copy(element.childrens);
        //};

        self.selectElement = function (element) {
          $scope.element = element;
          $scope.vm.model = {};

          CfBuilder.selectElement(element, function (result, error) {
            if (is.not.empty(result)) {
              $scope.vm.modelFields = result.modelFields;
            } else if (is.not.empty(error)) {
              alert(error.text);
            }
          });
        };

        self.onStop = function (element, index) {
          $scope.action = 'add';

          if (is.empty(element.model)) {
            $scope.headTitle = 'Create Element by Type: ' + element.title;
            self.selectElement(element);
            element.id = new Date().getTime();
            $scope.model.rootList[index] = angular.copy(element);
            $element.find('.cfdrag-modal').modal();
          }

          if (is.not.propertyDefined(element, 'model')) {
            alert("The element is incomplete, is recommended to don't use it");
            $scope.model.rootList.splice(index, 1);
          }
        };

        $scope.$watch('model.rootList', function (newValue, oldValue) {
          if (newValue.length > oldValue.length) {
            angular.forEach(newValue, function (value, index) {
              if (is.not.propertyDefined(value, 'id')) {
                self.onStop($scope.model.rootList[index], index);
              }
            });
          }

          // TODO: VCA: Optimizar esto.
          $scope.model.model = [];
          var listModels = [];
          angular.forEach($scope.model.rootList, function (value) {
            if (is.propertyDefined(value.model, 'data')) {
              listModels.push(value.model);
              $scope.newChange = true;
              $timeout(function () {
                $scope.newChange = false;
              }, 1000);
            }
          });

          $scope.model.model.push({
            "className" : "",
            "fieldGroup": listModels
          });
        }, true);

        $scope.previewShow = true;
        $timeout(function () {
          $scope.previewShow = false;
        }, 100);
        if ($scope.previewShow) {
          $scope.showAddPreview = false;
          $scope.showEditPreview = false;
          $scope.$watch('vm.model', function () {
            if ($scope.action == 'add') {
              if (is.propertyDefined($scope.element, 'model')) {
                CfBuilder.saveField($scope.element, $scope.vm.model, $scope.element.model, function (result, error) {
                  if (is.not.empty(result)) {
                    $scope.modelPreview = angular.copy(result.newField);
                    if (!is.propertyCount($scope.modelPreview, 0)) {
                      $scope.showAddPreview = true;
                    }
                  }
                });
              }
            }
          }, true);

          $scope.$watch('element.model.data.dataScheme', function (newValue) {
            if ($scope.action == 'edit') {
              if (is.propertyDefined($scope, 'element')) {
                var scopeModel = _.pluck($scope.model.rootList, 'model');

                CfBuilder.editField(scopeModel, $scope.model.rootList, $scope.element.model, newValue, function (result, error) {
                  if (is.not.empty(result)) {
                    $scope.modelPreview = angular.copy(result.editField)
                    if (!is.propertyCount($scope.modelPreview, 0)) {
                      $scope.showEditPreview = true;
                    }
                  }
                });
              }
            }
          }, true);
        }

        $scope.showEditModal = function (element, index) {
          $scope.element = angular.copy(element);
          $scope.action = 'edit';

          if (is.empty($scope.element.model)) {
            $scope.action = 'add';
            self.selectElement($scope.element);
          }

          $scope.headTitle = 'Edit Element Type: ' + $scope.element.title;
          $element.find('.cfdrag-modal').modal();
        };

        $scope.saveField = function () {

          CfBuilder.saveField($scope.element, $scope.vm.model, $scope.element.model, function (result, error) {
            if (is.not.empty(result)) {
              angular.forEach($scope.model.rootList, function (value, index) {
                if (value.id == $scope.element.id) {
                  $scope.model.rootList[index].model = angular.copy(result.newField);
                  $scope.vm.model = result.model;
                  $element.find('.cfdrag-modal').modal('hide');
                }
              });
            } else if (is.not.empty(error)) {
              alert(error.text);
            }
          });
        };

        $scope.editField = function (model, changeModelScheme) {

          var scopeModel = _.pluck($scope.model.rootList, 'model');

          CfBuilder.editField(scopeModel, $scope.model.rootList, model, changeModelScheme, function (result, error) {
            if (is.not.empty(result)) {
              $scope.model.rootList[result.index].model = angular.copy(result.editField);

              $scope.element = $scope.model.rootList[result.index];

              $element.find('.cfdrag-modal').modal('hide');
            }
          });
        };

        $scope.remove = function (index) {
          $scope.model.rootList.splice(index, 1);
        };

        $scope.decreasing = function (element, index) {
          $scope.element = angular.copy(element);

          var number = element.model.data.classes.numb;
          if (number > 0) {
            number = (number == 1) ? number : number - 1;

            $scope.model.rootList[index].model.data.classes = {
              text: 'col-xs-12 ' + 'col-sm-' + (number) + ' col-md-' + (number) + ' col-lg-' + (number),
              numb: number
            };

            var scopeModel = _.pluck($scope.model.rootList, 'model');

            CfBuilder.editField(scopeModel, $scope.model.rootList, element.model, element.model.data.dataScheme, function (result, error) {
              if (is.not.empty(result)) {
                $scope.model.rootList[result.index].model = angular.copy(result.editField);
                $scope.element = $scope.model.rootList[result.index];
              }
            });
          }
        };

        $scope.increasing = function (element, index) {
          $scope.element = angular.copy(element);

          var number = element.model.data.classes.numb;
          if (number < 12) {
            number = (number == 12) ? number : number + 1;

            $scope.model.rootList[index].model.data.classes = {
              text: 'col-xs-12 ' + 'col-sm-' + (number) + ' col-md-' + (number) + ' col-lg-' + (number),
              numb: number
            };

            var scopeModel = _.pluck($scope.model.rootList, 'model');

            CfBuilder.editField(scopeModel, $scope.model.rootList, element.model, element.model.data.dataScheme, function (result, error) {
              if (is.not.empty(result)) {
                $scope.model.rootList[result.index].model = angular.copy(result.editField);
                $scope.element = $scope.model.rootList[result.index];
              }
            });
          }
        };

      }]
    }
  });