/**
 * Module Directives specific to app.
 */
angular.module("cf")

/**
 * Directive to show form:
 * Example:
 * <div cfform-show-concept ng-model="model" fields="fields"></div>
 */
  .directive('cfformShowConcept', [function () {
    var strTemplate="";
    strTemplate += "<div class=\"\">";
    strTemplate += "  <div ng-repeat=\"concept in concepts track by $index\" class=\"cf-one-concept\" ng-class=\"concept.classes.text\">";
    strTemplate += "    <div class=\"page-header\">";
    strTemplate += "      <strong>{{ $index }}. {{ concept.name }}<\/strong>";
    strTemplate += "    <\/div>";
    strTemplate += "    <formly-form model=\"model\" fields=\"concept.data_concept.model\"><\/formly-form>";
    strTemplate += "  <\/div>";
    strTemplate += "<\/div>";

    return {
      restrict: 'E',
      replace : true,
      require : '?ngModel',

      scope: {
        model   : '=ngModel',
        concepts: '='
      },

      //templateUrl: '../dist/cf-form-show-concepts.html'
      template: strTemplate
    }
  }]);