/**
 * Element: Bootstrap DatePicker.
 * Directive
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', function ($rootScope) {
    var element = {
      parent     : ['all', 'datepicker'],
      title      : 'Bootstrap Date Picker',
      description: 'Date Picker from Angular Bootstrap UI'
    };

    element.model = {};

    /**
     * Function build, build and return json with field formly structure.
     *
     * @param modelJson
     * @param callback
     * @returns {{key: *, type: string, defaultValue: (*|fieldOptionsApiShape.defaultValue|string|field.defaultValue|g.defaultValue|Variable.defaultValue), templateOptions: {type: string, label: *, placeholder: (*|string|element.modelFields.templateOptions.placeholder|MainCtrl.fields.templateOptions.placeholder|Task._placeholder.placeholder|$scope.fields.placeholder), required: *, description: (*|string|element.modelFields.templateOptions.description|description|type.__apiCheckData.description|n.x.n.description)}}}
     */
    element.fbuild = function (modelJson, callback) {
      var field = {
        className      : (modelJson.className) ? modelJson.className : 'col-md-12',
        key            : modelJson.key,
        type           : 'bootstrap-ui-datepicker',
        templateOptions: {
          label      : modelJson.label,
          required   : modelJson.required,
          cfoptions  : modelJson,
          description: modelJson.description,
          options    : {}
        }
      };

      if (modelJson.addonLeftClass || modelJson.addonRightClass) {
        if (!!modelJson.addonLeftClass) {
          field.templateOptions.addonLeft = {
            class: modelJson.addonLeftClass
          }
        }

        if (!!modelJson.addonRightClass) {
          field.templateOptions.addonRight = {
            class: modelJson.addonRightClass
          }
        }

        setTimeout(function () {
          callback(field);
        }, 1000);
      }
      else {
        callback(field);
      }
    };

    /**
     * Model Fields.
     *
     * @type {{className: string, fieldGroup: *[]}[]}
     */

    element.modelFields = [
      {
        "className" : "row",
        "fieldGroup": [
          {
            className      : "col-xs-6",
            key            : 'key',
            type           : 'input',
            templateOptions: {
              type       : 'text',
              label      : 'Key model',
              placeholder: 'Key model',
              required   : true,
              description: 'The key to model value.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'label',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Display name',
              placeholder: 'Display name',
              required   : true,
              description: 'The name of the field label.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'minDate',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Min Date',
              placeholder: 'Example: 2013-06-22',
              required   : false,
              description: 'Defines the minimum available date.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'maxDate',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Max Date',
              placeholder: 'Example: 2016-06-22',
              required   : false,
              description: 'Defines the maximum available date.'
            }
          },
          //{
          //  className      : "col-xs-6",
          //  key            : 'showWeeks',
          //  type           : 'select',
          //  templateOptions: {
          //    label      : 'Show Weeks',
          //    options    : [
          //      {
          //        "name": true
          //      },
          //      {
          //        "name": false
          //      }],
          //    valueProp  : 'name',
          //    description: ' Whether to display week numbers.'
          //  }
          //},
          {
            className      : "col-xs-6",
            key            : 'showWeeks',
            type           : 'input',
            defaultValue   : true,
            templateOptions: {
              type       : 'checkbox',
              label      : 'Show Weeks?',
              placeholder: '',
              required   : false,
              description: 'Whether to display week numbers.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'startingDay',
            type           : 'input',
            templateOptions: {
              type       : 'number',
              label      : 'Starting Day',
              placeholder: '0',
              required   : false,
              description: 'Starting day of the week from 0-6 (0=Sunday, ..., 6=Saturday).'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'initDate',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Init Date',
              placeholder: '',
              required   : false,
              description: 'The initial date view when no model value is specified.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'formatDay',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Format Day',
              placeholder: 'dd',
              required   : false,
              description: 'Format of day in month.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'formatMonth',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Format Month',
              placeholder: 'MMMM',
              required   : false,
              description: 'Format of month in year.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'formatYear',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Format Year',
              placeholder: 'yyyy',
              required   : false,
              description: 'Format of year in year range.'
            }
          },

          {
            className: 'divider',
            template : ''
          },

          {
            className      : "col-xs-6",
            key            : 'datepickerPopup',
            type           : 'input',
            defaultValue   : 'yyyy-MM-dd',
            templateOptions: {
              type       : 'string',
              label      : 'Datepicker Popup',
              placeholder: 'yyyy-MM-dd',
              required   : false,
              description: 'The format for displayed dates.'
            }
          },
          //{
          //  className      : "col-xs-6",
          //  key            : 'showButtonBar',
          //  type           : 'select',
          //  templateOptions: {
          //    label      : 'Show Button Bar',
          //    options    : [
          //      {
          //        "name": true
          //      },
          //      {
          //        "name": false
          //      }],
          //    valueProp  : 'name',
          //    description: 'Whether to display a button bar underneath the datepicker.'
          //  }
          //},
          {
            className      : "col-xs-6",
            key            : 'showButtonBar',
            type           : 'input',
            defaultValue   : true,
            templateOptions: {
              type       : 'checkbox',
              label      : 'Show Button Bar?',
              placeholder: '',
              required   : false,
              description: 'Whether to display a button bar underneath the datepicker.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'currentText',
            type           : 'input',
            defaultValue   : 'Today',
            templateOptions: {
              type       : 'string',
              label      : 'Current Text',
              placeholder: 'Today',
              required   : false,
              description: 'The text to display for the current day button.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'clearText',
            type           : 'input',
            defaultValue   : 'Clear',
            templateOptions: {
              type       : 'string',
              label      : 'Clear Text',
              placeholder: 'Clear',
              required   : false,
              description: 'The text to display for the clear button.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'closeText',
            type           : 'input',
            defaultValue   : 'Done',
            templateOptions: {
              type       : 'string',
              label      : 'Close Text',
              placeholder: 'Done',
              required   : false,
              description: 'The text to display for the close button.'
            }
          },
          //{
          //  className      : "col-xs-6",
          //  key            : 'closeOnDateSelection',
          //  type           : 'select',
          //  templateOptions: {
          //    label      : 'Close On Date Selection',
          //    options    : [
          //      {
          //        "name": true
          //      },
          //      {
          //        "name": false
          //      }],
          //    valueProp  : 'name',
          //    description: 'Whether to close calendar when a date is chosen.'
          //  }
          //},
          {
            className      : "col-xs-6",
            key            : 'closeOnDateSelection',
            type           : 'input',
            defaultValue   : true,
            templateOptions: {
              type       : 'checkbox',
              label      : 'Close On Date Selection?',
              placeholder: '',
              required   : false,
              description: 'Whether to close calendar when a date is chosen.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'required',
            type           : 'input',
            templateOptions: {
              type       : 'checkbox',
              label      : 'Required?',
              placeholder: '',
              required   : false,
              description: 'Field is required?.'
            }
          },
          {
            className      : "col-xs-12",
            key            : 'description',
            type           : 'textarea',
            templateOptions: {
              type       : 'textarea',
              label      : 'Description',
              placeholder: 'Description',
              required   : false,
              description: 'Description of the field.',
              rows       : 4
            }
          }
        ]
      }
    ];

    $rootScope.$emit('cf:form:elements', element);
  }])