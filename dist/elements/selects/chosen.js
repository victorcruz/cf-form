/**
 * Element: AngularJS Chosen from chosen plugin.
 * Directive
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', function ($rootScope) {
    var element = {
      parent     : ['all', 'select'],
      title      : 'Chosen',
      description: 'Chosen plugin'
    };

    element.model = {};

    /**
     * Function build, build and return json with field formly structure.
     *
     * @param modelJson
     * @param callback
     * @returns {{key: *, type: string, defaultValue: (*|fieldOptionsApiShape.defaultValue|string|field.defaultValue|g.defaultValue|Variable.defaultValue), templateOptions: {type: string, label: *, placeholder: (*|string|element.modelFields.templateOptions.placeholder|MainCtrl.fields.templateOptions.placeholder|Task._placeholder.placeholder|$scope.fields.placeholder), required: *, description: (*|string|element.modelFields.templateOptions.description|description|type.__apiCheckData.description|n.x.n.description)}}}
     */
    element.fbuild = function (modelJson, callback) {
      var field = {
        className      : (modelJson.className) ? modelJson.className : 'col-md-12',
        key            : modelJson.key,
        type           : 'chosen',
        noFormControl  : true,
        templateOptions: {
          label      : modelJson.label,
          required   : modelJson.required,
          cfoptions  : modelJson,
          description: modelJson.description,
          list       : modelJson.list
        }
      };

      modelJson.list = modelJson.listDirective;

      if (angular.isDefined(modelJson.broadcastValue) && angular.isDefined(modelJson.onValue)) {
        if (modelJson.broadcastValue != '' && modelJson.onValue != '') {
          $rootScope.$broadcast(modelJson.broadcastValue, '');
        } else if (!!modelJson.listDirective && modelJson.listDirective.length > 0) {
          modelJson.list = modelJson.listDirective;
        } else {
          modelJson.list = [];
        }
      }

      $rootScope.$on(modelJson.onValue, function (event, items) {
        modelJson.list = items;
      });

      if (modelJson.addonLeftClass || modelJson.addonRightClass) {
        if (!!modelJson.addonLeftClass) {
          field.templateOptions.addonLeft = {
            class: modelJson.addonLeftClass
          }
        }

        if (!!modelJson.addonRightClass) {
          field.templateOptions.addonRight = {
            class: modelJson.addonRightClass
          }
        }

        setTimeout(function () {
          callback(field);
        }, 1000);
      }
      else {
        callback(field);
      }
    };

    /**
     * Model Fields.
     *
     * @type {{className: string, fieldGroup: *[]}[]}
     */

    element.modelFields = [
      {
        "className" : "row",
        "fieldGroup": [
          {
            className      : "col-xs-6",
            key            : 'key',
            type           : 'input',
            templateOptions: {
              type       : 'text',
              label      : 'Key model',
              placeholder: 'Key model',
              required   : true,
              description: 'The key to model value.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'label',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Display name',
              placeholder: 'Display name',
              required   : true,
              description: 'The name of the field label.'
            }
          },

          // --------------------------------------------------------------------

          {
            className: 'divider',
            template : ''
          },

          // --------------------------------------------------------------------

          {
            className      : "col-xs-12",
            key            : 'dataFactory',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Factory module.factory.function',
              placeholder: 'project.Person.getAll',
              required   : false,
              description: 'Get data by a factory, set name of module, factory and function to execute separated by point. Example: project.Person.getAll'
            }
          },

          // --------------------------------------------------------------------

          {
            className: 'divider',
            template : ''
          },

          // --------------------------------------------------------------------

          {
            className           : "col-xs-6",
            key                 : 'onValue',
            type                : 'input',
            templateOptions     : {
              type       : 'string',
              label      : 'On phrase',
              placeholder: 'chosen on',
              required   : false,
              description: 'Name of the event to get data by $on. Disabled when Factory module.factory.function is specified. (Example: chosen on)'
            },
            expressionProperties: {
              "templateOptions.disabled": "model.dataFactory"
            }
          },
          {
            className           : "col-xs-6",
            key                 : 'broadcastValue',
            type                : 'input',
            templateOptions     : {
              type       : 'string',
              label      : 'Broadcast phrase',
              placeholder: 'chosen broadcast',
              required   : false,
              description: 'Name of the event to send data by $broadcast. Disabled when Factory module.factory.function is specified or On phrase is not specified. (Example: chosen broadcast)'
            },
            expressionProperties: {
              "templateOptions.disabled": "!model.onValue || model.dataFactory"
            }
          },

          // --------------------------------------------------------------------

          {
            className: 'divider',
            template : ''
          },

          // --------------------------------------------------------------------

          {
            className      : "col-xs-12",
            key            : 'listDirective',
            template       : '<div cfselect ng-model="model[options.key]"></div>',
            templateOptions: {
              options    : {},
              label      : 'Values to select',
              description: 'Set data to show in select (chosen). Ignore when Factory module.factory.function or Broadcast phrase and On phrase is specified.'
            }
          },

          // --------------------------------------------------------------------

          {
            className: 'divider',
            template : ''
          },

          // --------------------------------------------------------------------

          {
            className      : "col-xs-6",
            key            : 'dataPlaceholder',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Placeholder',
              placeholder: 'Choose a value',
              required   : false,
              description: 'Text to show in placeholder.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'noResultText',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'No Data Text',
              placeholder: 'Could not find any values',
              required   : false,
              description: "Text to show when data doesn't exist."
            }
          },
          {
            className      : "col-xs-6",
            key            : 'valueFilter',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Display value filter',
              placeholder: 'name',
              required   : false,
              description: "Value to filter and show in Chosen. (Example: name)."
            }
          },
          {
            className      : "col-xs-6",
            key            : 'multiple',
            type           : 'input',
            defaultValue   : false,
            templateOptions: {
              type       : 'checkbox',
              label      : 'Select Multiple?',
              placeholder: '',
              required   : false,
              description: 'Default value: false'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'required',
            type           : 'input',
            templateOptions: {
              type       : 'checkbox',
              label      : 'Required?',
              placeholder: '',
              required   : false,
              description: 'Field is required?.'
            }
          },
          {
            className      : "col-xs-12",
            key            : 'description',
            type           : 'textarea',
            templateOptions: {
              type       : 'textarea',
              label      : 'Description',
              placeholder: 'Description',
              required   : false,
              description: 'Description of the field.',
              rows       : 4
            }
          }
        ]
      }
    ];

    $rootScope.$emit('cf:form:elements', element);
  }]);