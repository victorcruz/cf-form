/**
 * Element: Simple Select.
 * Complex select element html.
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', function ($rootScope) {
    var element = {
      parent     : ['all', 'select'],
      title      : 'Select',
      description: 'Simple select field'
    };

    element.model = {};

    /**
     * Function build, build and return json with field formly structure.
     *
     * @param modelJson
     * @param callback
     * @returns {{key: *, type: string, defaultValue: (*|fieldOptionsApiShape.defaultValue|string|field.defaultValue|g.defaultValue|Variable.defaultValue), templateOptions: {type: string, label: *, placeholder: (*|string|element.modelFields.templateOptions.placeholder|MainCtrl.fields.templateOptions.placeholder|Task._placeholder.placeholder|$scope.fields.placeholder), required: *, description: (*|string|element.modelFields.templateOptions.description|description|type.__apiCheckData.description|n.x.n.description)}}}
     */
    element.fbuild = function (modelJson, callback) {
      var field = {
        className      : (modelJson.className) ? modelJson.className : 'col-md-12',
        key            : modelJson.key,
        type           : 'select',
        //defaultValue   : modelJson.defaultValue,
        templateOptions: {
          //type       : 'string',
          label      : modelJson.label,
          //placeholder: modelJson.placeholder,
          required   : modelJson.required,
          description: modelJson.description,
          options    : modelJson.modelSelect,
          valueProp  : 'name'
          //minlength  : modelJson.min,
          //maxlength  : modelJson.max
        }
      };

      if (modelJson.addonLeftClass || modelJson.addonRightClass) {
        if (!!modelJson.addonLeftClass) {
          field.templateOptions.addonLeft = {
            class: modelJson.addonLeftClass
          }
        }

        if (!!modelJson.addonRightClass) {
          field.templateOptions.addonRight = {
            class: modelJson.addonRightClass
          }
        }

        setTimeout(function () {
          callback(field);
        }, 1000);
      }
      else {
        callback(field);
      }
    };

    /**
     * Model Fields.
     *
     * @type {{className: string, fieldGroup: *[]}[]}
     */

    element.modelFields = [
      {
        "className" : "row",
        "fieldGroup": [
          {
            className      : "col-xs-6",
            key            : 'key',
            type           : 'input',
            templateOptions: {
              type       : 'text',
              label      : 'Key model',
              placeholder: 'Key model',
              required   : true,
              description: 'The key to model value.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'label',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Display name',
              placeholder: 'Display name',
              required   : true,
              description: 'The name of the field label.'
            }
          },
          {
            className      : "col-xs-12",
            key            : 'modelSelect',
            template       : '<div cfselect ng-model="model[options.key]"></div>',
            templateOptions: {
              options: {},
              label  : 'Example Directive'
            }
          },
          //{
          //  className      : "col-xs-6",
          //  key            : 'defaultValue',
          //  type           : 'input',
          //  templateOptions: {
          //    type       : 'string',
          //    label      : 'Default value',
          //    placeholder: 'Default value',
          //    required   : false,
          //    description: 'Value by default of field.'
          //  }
          //},
          //{
          //  className      : "col-xs-6",
          //  key            : 'placeholder',
          //  type           : 'input',
          //  templateOptions: {
          //    type       : 'string',
          //    label      : 'Placeholder text',
          //    placeholder: 'Placeholder text',
          //    required   : false,
          //    description: 'The name of the field placeholder.'
          //  }
          //},
          //{
          //  className      : "col-xs-6",
          //  key            : 'min',
          //  type           : 'input',
          //  templateOptions: {
          //    type       : 'number',
          //    label      : 'Minimum of characters',
          //    placeholder: 'Minimum of characters',
          //    required   : false,
          //    description: ''
          //  }
          //},
          //{
          //  className      : "col-xs-6",
          //  key            : 'max',
          //  type           : 'input',
          //  templateOptions: {
          //    type       : 'number',
          //    label      : 'Maximum of characters',
          //    placeholder: 'Maximum of characters',
          //    required   : false,
          //    description: ''
          //  }
          //},
          {
            className      : "col-xs-6",
            key            : 'addonLeftClass',
            type           : 'input',
            templateOptions: {
              type       : 'text',
              label      : 'Input add-ons - Left',
              placeholder: 'glyphicon glyphicon-hand-left',
              required   : false,
              description: 'Use input add-ons to decorate your input fields. Ex: glyphicon glyphicon-hand-left',
              addonLeft  : {
                class: 'glyphicon glyphicon-hand-left'
              }
            }
          },
          {
            className      : "col-xs-6",
            key            : 'addonRightClass',
            type           : 'input',
            templateOptions: {
              type       : 'text',
              label      : 'Input add-ons - Right',
              placeholder: 'glyphicon glyphicon-hand-right',
              required   : false,
              description: 'Use input add-ons to decorate your input fields. Ex: glyphicon glyphicon-hand-right',
              addonRight : {
                class: 'glyphicon glyphicon-hand-right'
              }
            }
          },
          {
            className      : "col-xs-6",
            key            : 'required',
            type           : 'input',
            templateOptions: {
              type       : 'checkbox',
              label      : 'Required?',
              placeholder: '',
              required   : false,
              description: 'Field is required?.'
            }
          },
          {
            className      : "col-xs-12",
            key            : 'description',
            type           : 'textarea',
            templateOptions: {
              type       : 'textarea',
              label      : 'Description',
              placeholder: 'Description',
              required   : false,
              description: 'Description of the field.',
              rows       : 4
            }
          }
        ]
      }
    ];

    $rootScope.$emit('cf:form:elements', element);
  }])