/**
 * Module Models to work with indication template module in frontend.
 */
'use strict';

angular.module("cf")

  .factory('CfBuilder', [function () {

    return {

      /**
       * Get a root elements with childrens.
       *
       * @param rootElements
       * @param elements
       * @param callback
       */
      getElementsChildrens: function (rootElements, elements, callback) {
        var list = angular.copy(rootElements);

        angular.forEach(rootElements, function (value, key) {
          list[key].childrens = [];
          angular.forEach(elements, function (valueElement) {
            if (_.contains(valueElement.parent, value.element)) {
              list[key].childrens.push(valueElement);
            }
          });
        });

        callback(list);
      },

      /**
       * Execute when select a element, ex: Text Field (input)
       *
       * @param element
       * @param callback
       */
      selectElement: function (element, callback) {
        if (!!element.modelFields && angular.isArray(element.modelFields) && element.modelFields.length > 0) {
          // Set model fields to element selected.
          callback({
            modelFields: element.modelFields
          }, {});
        } else {
          callback({}, {
            type   : 'warning',
            text   : 'The element has no fields.',
            classes: 'text-warning'
          });
        }
      },

      /**
       * Instance model and save field.
       *
       * @param element   // $scope.element
       * @param vmModel   // $scope.vm.model
       * @param model     // $scope.model
       * @param callback  // function(result, error){}
       */
      saveField: function (element, vmModel, model, callback) {
        vmModel.className = 'cf-form-concept col-xs-12 col-sm-12 col-md-12 col-lg-12';
        element.fbuild(vmModel, function (newField) {
          if (!_.findWhere(model, {key: newField.key})) {
            newField.data = {
              id        : _.now(),
              scheme    : element,
              dataScheme: vmModel,
              classes   : {
                text: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
                numb: 12
              }
            };

            callback({
              newField: newField,
              model   : {}
            }, {});
          } else {
            callback({}, {
              type   : 'warning',
              text   : 'The element already exist',
              classes: 'text-warning'
            });
          }
        });
      },

      /**
       * Edit field.
       *
       * @param scopeModel        // $scope.model
       * @param rootElements      // $rootScope.elements
       * @param model             // model passed by param from template.
       * @param changeModelScheme // changeModelScheme passed by param from template.
       * @param callback
       */
      editField: function (scopeModel, rootElements, model, changeModelScheme, callback) {
        var element = _.findWhere(rootElements, {title: model.data.scheme.title});

        changeModelScheme.className = 'cf-form-concept ' + model.data.classes.text;

        element.fbuild(changeModelScheme, function (editField) {
          editField.data = {
            id        : model.data.id,
            scheme    : model.data.scheme,
            dataScheme: changeModelScheme,
            classes   : {
              text: model.data.classes.text,
              numb: model.data.classes.numb
            }
          };

          angular.forEach(scopeModel, function (value, index) {
            if (is.not.empty(value)) {
              if (value.data.id == model.data.id) {
                callback({
                  index    : index,
                  editField: editField
                }, {});
              }
            }
          });
        })
      },

      /**
       * Get API DATA from Custom Template of formly.
       *
       * @param dataString  Get data by a factory, set name of module, factory and function to execute separated by comma. Example: project.Person.getAll
       * @param callback
       */
      executeGetGenericData: function (dataString, callback) {
        if (dataString) {
          var splitData = dataString.split('.');
          if (splitData.length == 3) {
            var moduleName = splitData[0];
            var factoryName = splitData[1];
            var functionName = splitData[2];

            var injector = angular.injector(['ng', moduleName]);
            var factoryObject = injector.get(factoryName);
            callback(_.result(factoryObject, functionName));
          }
        }
      }
    };
  }]);