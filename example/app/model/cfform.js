/**
 * Module Models to work with indication template module in frontend.
 */
'use strict';

angular.module("project")

  .factory('Cfform', ['$http', function ($http) {
    var route = 'http://localhost:9993/cfforms';

    return {

      // Get all forms.
      getAll: function (callback) {
        $http.get(route, {})
          .success(function (result, status) {
            if (result) {
              callback(result);
            }
          });
      },

      // Get one form by id.
      getOne: function (id, callback) {
        $http.get(route + '/' + id, {})
          .success(function (result) {
            if (result) {
              var jsonObject = result;
              jsonObject.data_form = angular.fromJson(result.data_form);
              callback(jsonObject);
            }
          });
      },

      // Add new form.
      add   : function (jsonObject, callback) {
        var sendObject = angular.copy(jsonObject);

        sendObject.data_form = angular.toJson(jsonObject.data_form);

        $http.post(route, sendObject)
          .success(function (result) {
            //alert('Created: ' + result);
            callback(result);
          })
          .error(function (error) {
            alert('Error');
          });
      },

      // Edit a form by object.
      edit  : function (jsonObject) {
        var sendObject = angular.copy(jsonObject);
        sendObject.data_form = angular.toJson(jsonObject.data_form);

        $http.put(route + '/' + sendObject.id, sendObject)
          .success(function (result) {
            alert('Edited: ' + result);
          })
          .error(function (error) {
            alert('Error');
          })
      },

      // Remove a form by id.
      remove: function (id, callback) {
        $http.delete(route + '/' + id, {})
          .success(function () {
            callback();
          })
          .error(function (error) {
            console.log('Error');
          });
      }
    }
  }]);